public class cat{
	private String color;
	private double weight;
	private String name;

	public void meows(String name){
			System.out.println(name+" meows loudly!");
	}
	
	public void inShape(double weight, String name){
		if(weight > 15){
			System.out.println(name+" is obese at the weight of: "+weight);
		}else if(weight < 10){
			System.out.println(name+" is underweight at the weight of: "+weight);
		}else{
			System.out.println(name+" is healthy at the weight of: "+weight);
		}
	}
	
	public void eat(int amountAte, double weight){
		if(helperCheck(amountAte)){
			this.weight = weight + amountAte;
			System.out.println("Your cat current weight is "+this.weight);
		}else{
			System.out.println("Put a valid number and don't let your cat starve");
		}
	}
	
	public boolean helperCheck(int amount){
		boolean helperReturn = true;
		if(amount < 0){
			helperReturn = false;
		}
		return helperReturn;
	}
	
	//Getter methods
	public String getColor(){
		return this.color;
	}
	public double getWeight(){
		return this.weight;
	}
	public String getName(){
		return this.name;
	}
	
	
	//Setter methods
	public void setColor(String newColor){
		this.color = newColor;
	}
	public void setWeight(double newWeight){
		this.weight = newWeight;
	}
	
	
	//constructor
	public cat(String name){
		this.name = name;
		//this.color=color;
		//this.weight=weight;
	}
}